# Beamer multilingual template

Simple Beamer Darmstadt theme with multilingual support. The template consists of:

* multilanguage.sty Multi language support thank to https://github.com/ejs-ejs/multilanguage

* myAppendix.tex Extra material, Bibliography, Disclaimer of liability, License

* myDefinitions.tex Title, Author, Contacts, Date, etc.

* myFrontPage.tex Custom command, equation, symbols, etc.

* myPackages.sty Included packages

* myPresentation.tex **Main**

* mySlides.tex Slides